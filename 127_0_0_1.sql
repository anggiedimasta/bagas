-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2015 at 07:46 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_bagas`
--
CREATE DATABASE IF NOT EXISTS `db_bagas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_bagas`;

-- --------------------------------------------------------

--
-- Table structure for table `tb_brand`
--

CREATE TABLE IF NOT EXISTS `tb_brand` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_brand`
--

INSERT INTO `tb_brand` (`id`, `nama`) VALUES
(1, 'Nike'),
(2, 'Oakley'),
(3, 'Ray-Ban'),
(4, 'Waith');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cart`
--

CREATE TABLE IF NOT EXISTS `tb_cart` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `tanggal_masuk` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_user` int(10) NOT NULL,
  `destination` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_cart`
--

INSERT INTO `tb_cart` (`id`, `tanggal_masuk`, `id_user`, `destination`, `status`) VALUES
(1, '2015-08-04 15:22:29', 28, 'Lowokdoro', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_cart_product`
--

CREATE TABLE IF NOT EXISTS `tb_cart_product` (
  `id_cart` int(6) NOT NULL,
  `id_product` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cart_product`
--

INSERT INTO `tb_cart_product` (`id_cart`, `id_product`) VALUES
(1, 1),
(1, 2),
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE IF NOT EXISTS `tb_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`id`, `nama`) VALUES
(1, 'Pilot'),
(2, 'Retro'),
(3, 'Sports'),
(4, 'Oversized'),
(5, 'Glasses');

-- --------------------------------------------------------

--
-- Table structure for table `tb_confirm`
--

CREATE TABLE IF NOT EXISTS `tb_confirm` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_user` int(10) NOT NULL,
  `key` varchar(32) NOT NULL,
  `email` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE IF NOT EXISTS `tb_product` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(120) NOT NULL,
  `id_brand` int(10) NOT NULL,
  `id_category` int(10) NOT NULL,
  `harga` int(10) NOT NULL,
  `warna` varchar(15) NOT NULL,
  `quantity` int(3) NOT NULL,
  `size` varchar(10) NOT NULL,
  `weight` float NOT NULL,
  `spesifikasi` text NOT NULL,
  `image` varchar(250) DEFAULT 'images/product-details/1.jpg',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`id`, `nama`, `id_brand`, `id_category`, `harga`, `warna`, `quantity`, `size`, `weight`, `spesifikasi`, `image`) VALUES
(1, 'Ray-Ban 3025 112/19 Sunglasses (Green/Gold)', 3, 1, 750000, 'Green/Gold', 4, '14x1.6x5', 0.3, 'Sunglasses<br/>Material: Metal<br/>Lens Color: Green<br/> Frame Color: Gold<br/>Lens Size: 58 mm<br/>', 'images/shop/ray-ban-3025-112-19-sunglasses-green-gold-3951-461944-1.jpg'),
(2, 'Wayth 8006 Stylish Sunglasses (Matte Black)', 4, 4, 1250000, 'Matte Black', 7, '18x8x4', 0.3, 'Good Quantity<br/>Big Discount<br/>Fine Material<br/>New Design<br/>New Arrival<br/>Fashion<br/>', 'images/shop/wayth-8006-matte-black-pc-frame-gray-pc-lens-stylish-glasses-sunglasses-matte-black-0258-850676.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_settings`
--

CREATE TABLE IF NOT EXISTS `tb_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  `value` smallint(3) DEFAULT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_settings`
--

INSERT INTO `tb_settings` (`id`, `nama`, `value`, `text`) VALUES
(1, 'follow up cart items <= 10', 3, 'Hallo, pengunjung yang terhormat bagaimana kabarnya? Apakah anda tidak ingin menyelesaikan cart transaction ini. Buruan dapatkan product-nya sebelum kehabisan!');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(35) NOT NULL,
  `password` varchar(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `nama_lengkap` varchar(35) NOT NULL,
  `alamat` text,
  `telepon` int(12) DEFAULT NULL,
  `kode_pos` int(5) DEFAULT NULL,
  `provinsi` varchar(20) DEFAULT NULL,
  `kota` varchar(20) DEFAULT NULL,
  `security_question` varchar(50) DEFAULT NULL,
  `security_answer` varchar(20) DEFAULT NULL,
  `access` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `password`, `is_active`, `nama_lengkap`, `alamat`, `telepon`, `kode_pos`, `provinsi`, `kota`, `security_question`, `security_answer`, `access`) VALUES
(1, 'anggie2dimasta@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 0, 'Anggie Putra Dimasta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 1, 'Administrator', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(27, 'anggieblablabla@yahoo.com', 'f1601e9a00dd6b861e9fc09ee040dbcf', 1, 'stfu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(28, 'anggiedimasta@gmail.com', 'd9ddd948d08262ac920ab7c01277510e', 1, 'stfu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(29, 'anggieblablabla2@gmail.com', 'd9ddd948d08262ac920ab7c01277510e', 1, 'stfu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
