<div class="row">
	<ol class="breadcrumb">
		<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
		<li class="active">Category</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Category</h1>
	</div>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Category Table</div>
			<div class="panel-body">
				<table id="tbproduct" data-toggle="table" data-url="configs/mysqltojsoncategory.php"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
					<thead>
					<tr>
						<th data-field="state" data-checkbox="true"></th>
						<th data-field="id" data-sortable="true" >Category ID</th>
						<th data-field="nama" data-sortable="true">Category Name</th>
					</tr>
					</thead>
				</table>
				<div id="toolbar">
					<button id="delete" class="btn btn-danger pull-right">remove</button>
					<button id="edit" class="btn btn-warning pull-right" style="margin-right: 5px;">edit</button>
					<button id="add" class="btn btn-success pull-right" style="margin-right: 5px;">add</button>
				</div>
			</div>
		</div>
	</div>
</div><!--/.row-->
<script>
	$(function () {
		$('#hover, #striped, #condensed').click(function () {
			var classes = 'table';

			if ($('#hover').prop('checked')) {
				classes += ' table-hover';
			}
			if ($('#condensed').prop('checked')) {
				classes += ' table-condensed';
			}
			$('#table-style').bootstrapTable('destroy')
				.bootstrapTable({
					classes: classes,
					striped: $('#striped').prop('checked')
				});
		});
	});

	function rowStyle(row, index) {
		var classes = ['active', 'success', 'info', 'warning', 'danger'];

		if (index % 2 === 0 && index / 2 < classes.length) {
			return {
				classes: classes[index / 2]
			};
		}
		return {};
	}
</script>
<script>
    var $table = $('#tbproduct'),
        $button = $('#delete');
    $(function () {
        $button.click(function () {
            var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
                return row.id;
            });
			var url = 'functions/deleteproduct.php';
			var data = 'id=' + ids.join(',');

			$.ajax({
				url: url,
				data: data,
				cache: false,
				error: function (e) {
					alert(e);
				},
				success: function () {
					$('#users-table').bootstrapTable('remove', {
						field: 'id',
						values: ids
					});
				}
			});
        });
    });
</script>