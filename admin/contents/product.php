<div class="row">
	<ol class="breadcrumb">
		<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
		<li class="active">Product</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Product</h1>
	</div>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Product Table</div>
			<div class="panel-body">
				<table id="tbproduct" data-toggle="table" data-url="configs/mysqltojsonproduct.php"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
					<thead>
					<tr>
						<th data-field="state" data-checkbox="true"></th>
						<th data-field="id" data-sortable="true" >Item ID</th>
						<th data-field="nama" data-sortable="true">Item Name</th>
						<th data-field="id_brand"  data-sortable="true">Brand ID</th>
						<th data-field="id_category" data-sortable="true">Category ID</th>
						<th data-field="harga" data-sortable="true">Price</th>
						<th data-field="warna" data-sortable="true">Color</th>
						<th data-field="size" data-sortable="true">Size</th>
						<th data-field="weight" data-sortable="true">Weight</th>
						<th data-field="spesifikasi" data-sortable="true">Specification</th>
						<th data-field="image" data-sortable="true">Image</th>
					</tr>
					</thead>
				</table>
				<div id="toolbar">
					<button id="delete" class="btn btn-danger pull-right" onClick="removeRow()">remove</button>
					<button id="edit" class="btn btn-warning pull-right" style="margin-right: 5px;">edit</button>
					<button id="add" class="btn btn-success pull-right" style="margin-right: 5px;">add</button>
				</div>
			</div>
		</div>
	</div>
</div><!--/.row-->
<script>
	$(function () {
		$('#hover, #striped, #condensed').click(function () {
			var classes = 'table';

			if ($('#hover').prop('checked')) {
				classes += ' table-hover';
			}
			if ($('#condensed').prop('checked')) {
				classes += ' table-condensed';
			}
			$('#table-style').bootstrapTable('destroy')
				.bootstrapTable({
					classes: classes,
					striped: $('#striped').prop('checked')
				});
		});
	});

	function rowStyle(row, index) {
		var classes = ['active', 'success', 'info', 'warning', 'danger'];

		if (index % 2 === 0 && index / 2 < classes.length) {
			return {
				classes: classes[index / 2]
			};
		}
		return {};
	}
</script>
<script>
    function removeRow() {

		var selects = $('#tbproduct').bootstrapTable('getSelections');
			ids = $.map(selects, function (row) {
				return row.id;
			});

		var url = 'functions/deleteproduct.php';
		var data = 'id=' + ids;
		var type = 'GET';
		alert(data);
		
		$.ajax({
			type: type,
			url: url,
			data: data,
			error: function () {
				alert(data);
			},
			success: function() {
				$('#tbproduct').bootstrapTable('remove', {
					field: 'id',
					values: ids
				});
			}
		});
	}
</script>