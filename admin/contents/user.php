<div class="row">
	<ol class="breadcrumb">
		<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
		<li class="active">User</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">User</h1>
	</div>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">User Table</div>
			<div class="panel-body">
				<table id="tbproduct" data-toggle="table" data-url="configs/mysqltojsonuser.php"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
					<thead>
					<tr>
						<th data-field="state" data-checkbox="true"></th>
						<th data-field="id" data-sortable="true" >User ID</th>
						<th data-field="email" data-sortable="true">Email</th>
						<th data-field="nama_lengkap" data-sortable="true">Fullname</th>
						<th data-field="alamat" data-sortable="true">Address</th>
						<th data-field="kota" data-sortable="true">City</th>
						<th data-field="provinsi" data-sortable="true">Province</th>
						<th data-field="telepon" data-sortable="true">Phone</th>
						<th data-field="kode_pos" data-sortable="true">Postal Code</th>
						<th data-field="is_active" data-sortable="true">Status</th>
					</tr>
					</thead>
				</table>
				<div id="toolbar">
					<button id="delete" class="btn btn-danger pull-right">remove</button>
					<button id="edit" class="btn btn-warning pull-right" style="margin-right: 5px;">edit</button>
					<button id="add" class="btn btn-success pull-right" style="margin-right: 5px;">add</button>
				</div>
			</div>
		</div>
	</div>
</div><!--/.row-->
<script>
	$(function () {
		$('#hover, #striped, #condensed').click(function () {
			var classes = 'table';

			if ($('#hover').prop('checked')) {
				classes += ' table-hover';
			}
			if ($('#condensed').prop('checked')) {
				classes += ' table-condensed';
			}
			$('#table-style').bootstrapTable('destroy')
				.bootstrapTable({
					classes: classes,
					striped: $('#striped').prop('checked')
				});
		});
	});

	function rowStyle(row, index) {
		var classes = ['active', 'success', 'info', 'warning', 'danger'];

		if (index % 2 === 0 && index / 2 < classes.length) {
			return {
				classes: classes[index / 2]
			};
		}
		return {};
	}
</script>
<script>
    var $table = $('#tbproduct'),
        $button = $('#delete');
    $(function () {
        $button.click(function () {
            var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
                return row.id;
            });
			var url = 'functions/deleteproduct.php';
			var data = 'id=' + ids.join(',');

			$.ajax({
				url: url,
				data: data,
				cache: false,
				error: function (e) {
					alert(e);
				},
				success: function () {
					$('#users-table').bootstrapTable('remove', {
						field: 'id',
						values: ids
					});
				}
			});
        });
    });
</script>