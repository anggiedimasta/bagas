<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Epik!</span>Eyewear</a>
				<ul class="user-menu">
					<a href="#" style="margin-right:10px;"><span class="glyphicon glyphicon-user"></span> <?php echo $login_session; ?></a>
					<a href="../signout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>