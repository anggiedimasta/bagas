<?php 
	$active = '';
	if(isset($_GET['product'])) {
		$active = 'product';
	} else if(isset($_GET['category'])) {
		$active = 'category';
	} else if(isset($_GET['brand'])) {
		$active = 'brand';
	} else if(isset($_GET['user'])) {
		$active = 'user';
	} else if(isset($_GET['settings'])) {
		$active = 'settings';
	} else {
		$active = 'home';
	}
?>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li <?php if($active == 'home') { ?>class="active" <?php } ?>><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
			<li <?php if($active == 'product') { ?>class="active" <?php } ?>><a href="index.php?product"><span class="glyphicon glyphicon-th"></span> Product</a></li>
			<li <?php if($active == 'category') { ?>class="active" <?php } ?>><a href="index.php?category"><span class="glyphicon glyphicon-stats"></span> Category</a></li>
			<li <?php if($active == 'brand') { ?>class="active" <?php } ?>><a href="index.php?brand"><span class="glyphicon glyphicon-list-alt"></span> Brand</a></li>
			<li <?php if($active == 'user') { ?>class="active" <?php } ?>><a href="index.php?user"><span class="glyphicon glyphicon-pencil"></span> User</a></li>
			<li <?php if($active == 'settings') { ?>class="active" <?php } ?>><a href="index.php?settings"><span class="glyphicon glyphicon-info-sign"></span> Settings</a></li>
			<!--<li class="parent ">
				<a href="#">
					<span class="glyphicon glyphicon-list"></span> Dropdown <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span> 
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="#">
							<span class="glyphicon glyphicon-share-alt"></span> Sub Item 1
						</a>
					</li>
					<li>
						<a class="" href="#">
							<span class="glyphicon glyphicon-share-alt"></span> Sub Item 2
						</a>
					</li>
					<li>
						<a class="" href="#">
							<span class="glyphicon glyphicon-share-alt"></span> Sub Item 3
						</a>
					</li>
				</ul>
			</li>-->
		</ul>
	</div><!--/.sidebar-->