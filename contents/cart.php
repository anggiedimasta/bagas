	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php 
							if (isset($_GET['act']) && ($_GET['act'] == 'add') && isset($_GET['id'])) 
							{
								$product = $_GET['id'];
								$idcart = mysql_query("SELECT `id` FROM `tb_cart` WHERE `tb_cart`.`id_user`=".$login_session_id);
								$cart = mysql_fetch_array($idcart, MYSQL_NUM);
								$query = mysql_query("INSERT INTO `tb_cart_product` VALUES(".$cart[0].", ".$product.")");	
							} 
							$query = mysql_query("SELECT DISTINCT * FROM `tb_cart`, `tb_product`, `tb_cart_product` WHERE `tb_cart`.`id_user`=".$login_session_id." AND `tb_cart`.`id`=`tb_cart_product`.`id_cart` AND `tb_product`.`id`=`tb_cart_product`.`id_product`");
							if ($query) 
							{
								while ($row = mysql_fetch_array($query, MYSQL_BOTH)) 
								{
									$query2 = mysql_query("SELECT COUNT(*) FROM `tb_cart`, `tb_cart_product` WHERE `tb_cart`.`id_user`=".$login_session_id." AND ".$row[0]."=`tb_cart_product`.`id_cart` AND `tb_cart_product`.`id_product`=".$row['id_product']);
									$row2 = mysql_fetch_array($query2, MYSQL_NUM);
						?>
									<tr>
										<td class="cart_product">
											<a href=""><img src="<?php echo $row['image']; ?>" alt="" height="100px" style="margin-right:50px;"></a>
										</td>
										<td class="cart_description">
											<h4><a href=""><?php echo $row[6]; ?></a></h4>
											<p>Web ID: <?php echo $row[5]; ?></p>
										</td>
										<td class="cart_price">
											<p>Rp <?php echo number_format($row['harga'], 2, ',', '.'); ?></p>
										</td>
										<td class="cart_quantity">
											<div class="cart_quantity_button">
												<a class="cart_quantity_up" href=""> + </a>
												<input class="cart_quantity_input" type="text" name="quantity" value="<?php echo $row2[0]; ?>" autocomplete="off" size="2">
												<a class="cart_quantity_down" href=""> - </a>
											</div>
										</td>
										<td class="cart_total">
											<p class="cart_total_price">Rp <?php echo number_format($row['harga']*$row2[0] , 2, ',', '.'); ?></p>
										</td>
										<td class="cart_delete">
											<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
										</td>
									</tr>
						<?php 
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>$59</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>$61</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
