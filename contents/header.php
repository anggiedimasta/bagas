	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +62 8121 694 5084</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> admin@epikeyewear.com</a></li>
								<?php 
									if(isset($login_session_id)) {
										$querycarttime = mysql_query("SELECT `tanggal_masuk`, `id` FROM `tb_cart` WHERE `id_user`=".$login_session_id);
										$querysettingstime = mysql_query("SELECT * FROM `tb_settings` WHERE `id`=1");
										$carttime = mysql_fetch_array($querycarttime, MYSQL_NUM);
										$settingstime = mysql_fetch_array($querysettingstime, MYSQL_ASSOC);
										$timestamp = strtotime($carttime[0]) + ($settingstime['value']*60*60);
										$time = date('Y/m/d H:i:s', $timestamp);
								?>
								<li><a data-countdown="<?php echo $time; ?>"></a></li>
								<?php 
									} ?>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="images/clearlycontacts_man.png" alt="" width="150px" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-user"></i> <?php 
																												if(isset($login_session)){
																													echo $login_session; 
																												} else {
																													echo "Guest";
																												}?></a></li>
								<li><a href="?checkout"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="?cart"><i class="fa fa-shopping-cart"></i> Cart</a></li>
								<li><a href="<?php 
																												if(isset($login_session)){
																													echo "signout.php"; 
																												} else {
																													echo "?signin";
																												}
														?>
													"><i class="fa fa-lock"></i> <?php 
																												if(isset($login_session)){
																													echo "Sign Out"; 
																												} else {
																													echo "Sign In";
																												}?></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<?php 
									$active = '';
									if(isset($_GET['products'])) {
										$active = 'products';
									} else if(isset($_GET['signin'])) {
										$active = 'signin';
									} else if(isset($_GET['contact'])) {
										$active = 'contact';
									} else if(isset($_GET['help'])) {
										$active = 'help';
									} else {
										$active = 'home';
									}
								?>
								<li><a href="index.php" <?php if($active == 'home') { ?>class="active" <?php } ?>>Home</a></li>
                                <li><a href="?products" <?php if($active == 'products') { ?>class="active" <?php } ?>>Products</a></li>
								<li><a href="?contact" <?php if($active == 'contact') { ?>class="active" <?php } ?>>Contact</a></li>
								<li><a href="?help" <?php if($active == 'help') { ?>class="active" <?php } ?>>Help</a></li>
								<!--<li class="dropdown"><a href="#">Example<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
										<li><a href="product-details.php">Product Details</a></li> 
										<li><a href="checkout.php">Checkout</a></li> 
										<li><a href="cart.php">Cart</a></li> 
										<li><a href="signin.php">Sign In</a></li> 
										<li><a href="blog.php">Blog List</a></li>
										<li><a href="blog-single.php">Blog Single</a></li>
										<li><a href="404.php">404</a></li>
                                    </ul>
                                </li>--> 
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	