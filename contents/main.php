				<section>
		<div class="container">
			<div class="row">
				<?php 
					include("sidebar.php");
				?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
							<?php
								$query = mysql_query("SELECT `id`, `nama`, `harga`, `image` FROM `tb_product`");
								if ($query) {
									$i = 0;
									while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) {
									
							?>
										<div class="col-sm-4">
											<div class="product-image-wrapper">
												<div class="single-products">
													<div class="productinfo text-center">
														<img src="<?php echo $row['image']; ?>" alt="" />
														<h2>Rp <?php echo number_format($row['harga'], 2, ',', '.'); ?></h2>
														<p><?php echo $row['nama']; ?></p>
														<a href="?cart&act=add&id=<?php echo $row['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Quick add to cart</a>
													</div>
													<div class="product-overlay">
														<div class="overlay-content">
															<h2>Rp <?php echo number_format($row['harga'], 2, ',', '.'); ?></h2>
															<p><?php echo $row['nama']; ?></p>
															<a href="?products&id=<?php echo $row['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-info"></i>View Details</a>
															<a href="?cart&act=add&id=<?php echo $row['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Quick add to cart</a>
														</div>
													</div>
													<?php
														if ($i == 0) {
															?>
																<img src="images/home/new.png" class="new" alt="" />
															<?php
														} else if ($i == 1) {
															?>
																<img src="images/home/sale.png" class="new" alt="" />
															<?php
														}
														$i++;
													?>
												</div>
											</div>
										</div>
							<?php
									}
								}
							?>
					</div><!--features_items-->
					
					<div class="category-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
							<?php
								$query = mysql_query("SELECT `nama` FROM `tb_category`");
								if ($query) {
									$i = 0;
									while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) {?>
										<li <?php if ($i == 0){?> class="active"<?php } ?>><a href="#<?php echo $row['nama']; ?>" data-toggle="tab"><?php echo $row['nama']; ?></a></li>
									<?php 
										$i++;
									}
								}
							?>
							</ul>
						</div>
						<div class="tab-content">
							<?php
								$query = mysql_query("SELECT `id`, `nama`, `id_category`, `harga`, `image` FROM `tb_product`");
								if ($query) {
									$i = 0;
									while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) {
										$query2 = mysql_query("SELECT `nama` FROM `tb_category` WHERE `id`='".$row['id_category']."'");
										$namacat = mysql_fetch_array($query2, MYSQL_ASSOC)
							?>
								<div class="tab-pane fade active in" id="<?php echo $namacat['nama']; ?>">
									<div class="col-sm-3">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="<?php echo $row['image']; ?>" alt="" />
													<h2>Rp <?php echo number_format($row['harga'], 2, ',', '.'); ?></h2>
													<p><?php echo $row['nama']; ?></p>
													<a href="?cart&act=add&id=<?php echo $row['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Quick add to cart</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php
									}
								}
							?>
						</div>
					</div><!--/category-tab-->
					
					<!--<div class="recommended_items">
						<h2 class="title text-center">recommended items</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend1.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend2.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend3.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="item">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend1.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend2.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend3.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div>recommended_items-->
					
				</div>
				</div>
				</div>
				</section>