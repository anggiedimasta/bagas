<?php 
	$id = $_GET['id'];
	$query = mysql_query("SELECT * FROM `tb_product`, `tb_category`, `tb_brand` WHERE `tb_product`.`id`= $id AND `tb_product`.`id_category`=`tb_category`.`id` AND `tb_product`.`id_brand`=`tb_brand`.`id`");
	$row = mysql_fetch_array($query, MYSQL_BOTH);
?>
<div class="product-details"><!--product-details-->
	<div class="col-sm-5">
		<div class="view-product">
			<img src="<?php echo $row['image']; ?>" alt="" style="height:100%;" />
			<h3>Hover to zoom</h3>
		</div>
		<div id="similar-product" class="carousel slide" data-ride="carousel">
			
			  <!-- Wrapper for slides -->
				<!--<div class="carousel-inner">
					<div class="item active">
					  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
					</div>
					<div class="item">
					  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
					</div>
					<div class="item">
					  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
					</div>
					
				</div>-->

			  <!-- Controls -->
			  <!--<a class="left item-control" href="#similar-product" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			  </a>
			  <a class="right item-control" href="#similar-product" data-slide="next">
				<i class="fa fa-angle-right"></i>
			  </a>-->
		</div>

	</div>
	<div class="col-sm-7">
		<form id="product-details-cart" method="post" action="actions/addtocart.php">
			<div class="product-information"><!--/product-information-->
				<img src="images/product-details/new.jpg" class="newarrival" alt="" />
				<h2><?php echo $row[1]; ?></h2>
				<p>Web ID: <?php echo $row[0]; ?></p>
				<span>
					<span>Rp <?php echo number_format($row['harga'], 2, ',', '.'); ?></span>
				</span>
				<p><b>Availability:</b> <?php echo $row['quantity']; ?> item(s)</p>
				<span>
					<label>Quantity:</label>
					<input type="hidden" value="<?php echo $row[0]; ?>" name="id" />
					<input type="text" value="<?php echo $row['quantity']; ?>" name="quantity" />
					<button type="submit" class="btn btn-fefault cart" name="addtocart">
						<i class="fa fa-shopping-cart"></i>
						Add to cart
					</button>
				</span>
				<p><b>Category:</b> <?php echo $row[12]; ?></p>
				<p><b>Brand:</b> <?php echo $row['nama']; ?></p>
				<p><b>Color:</b> <?php echo $row['warna']; ?></p>
				<p><b>Size:</b> <?php echo $row['size']; ?></p>
				<p><b>Weight:</b> <?php echo $row['weight']; ?></p>
				<p><b>Details:</b> <?php echo $row['spesifikasi']; ?></p>
			</div><!--/product-information-->
		</form>
	</div>
</div><!--/product-details-->					
				