<?php

function format_email($info, $format){

	//set the root
	$root = $_SERVER['DOCUMENT_ROOT'].'/bagas';

	//grab the template content
	$template = file_get_contents($root.'/contents/email/followupcart_template.'.$format);
			
	//replace all the tags
	$template = ereg_replace('{NAME}', $info['name'], $template);
	$template = ereg_replace('{EMAIL}', $info['email'], $template);
	$template = ereg_replace('{TEXT}', $info['text'], $template);
	$template = ereg_replace('{SITEPATH}','http://127.0.0.1/bagas', $template);
		
	//return the html of the template
	return $template;

}

//send the welcome letter
function send_email($info){
		
	//format each email
	$body = format_email($info,'html');
	$body_plain_txt = format_email($info,'txt');
	$subject = 'Epik! Eyewear | '.$info['subject'];

	//setup the mailer
	// $transport = Swift_MailTransport::newInstance();
	$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
  ->setUsername('epikeyewearshop@gmail.com')
  ->setPassword('epikeyewearshop123');
	$mailer = Swift_Mailer::newInstance($transport);
	$message = Swift_Message::newInstance();
	$message ->setSubject($subject);
	$message ->setFrom(array('epikeyewearshop@gmail.com' => 'Epik! Eyewear'));
	$message ->setTo(array($info['email'] => $info['name']));
	
	$message ->setBody($body_plain_txt);
	$message ->addPart($body, 'text/html');
			
	$result = $mailer->send($message);
	
	return $result;
	
}