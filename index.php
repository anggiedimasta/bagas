<?php
	include("configs/session.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Epik! Eyewear</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/bootstrap-editable.css" rel="stylesheet"/>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/clearlycontacts_man.png">
</head><!--/head-->

<body>
	<?php
		include("contents/header.php");
		if (isset($_GET['products'])) {
			include("contents/products.php");
		} else if (isset($_GET['signin'])){
			include("contents/signin.php");
		} else if (isset($_GET['contact'])){
			include("contents/contact.php");
		} else if (isset($_GET['cart'])){
			include("contents/cart.php");
		} else {
			include("contents/slider.php");
			include("contents/main.php");
		}
		include("contents/footer.php");
	?>

  
    <script src="js/jquery.js"></script>
    <script src="js/jquery.countdown.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
	<script src="js/bootstrap-editable.min.js"></script>
	<script>
		$('[data-countdown]').each(function() {
		 var $this = $(this), finalDate = $(this).data('countdown');
		 $this.countdown(finalDate, function(event) {    
			 $this.html(event.strftime(''
			 + '<span>%d:</span>'
			 + '<span>%H:</span>'
			 + '<span>%M:</span>'
			 + '<span>%S</span>')); 
		 }); 
		 $(this).on('finish.countdown', function(event) {
			// alert('Example Callback text or add an alert or function up to you');
			var url = 'functions/followupcart.php';
			var data = 'id=' + <?php echo $carttime[1]; ?> + '&interval=' + <?php echo $settingstime['value']; ?> + '&setid=' + <?php echo $settingstime['id']; ?>;
			var type = 'GET';
			alert(url+data);
			
			$.ajax({
				type: type,
				url: url,
				data: data,
				error: function () {
					alert(data);
				},
				success: function() {
					//do something success
				}
			});
		 });
		});
	</script>
</body>
</html>